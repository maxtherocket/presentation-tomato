![fill](images/splash.jpg)

---

# FRONTEND

- Ionic Framework, Cordova
- AngularJS
- GSAP (TweenLite)
- Less
- Sketch + Avocode

---

# Ionic Framework

![left](images/ionic.jpg)

- Open source
- AngularJS-based
- Set of UI / Components
- Command-line tools
- View App
- Icon/splash image generator
- Platform (Push, Build)


---

# BACKEND

- Ruby on Rails
- Elasticsearch
- PostgreSQL
- Redis (Notifications, Cron Tasks)
- Amazon S3
- Cloud66 + Digital Ocean
